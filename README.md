# What's this?

This is a markdown parser by pure [Kotlin](https://kotlinlang.org/).

This parser can use for **EVERY** JVM project because Kotlin is JVM language too!

This parser support for [GitHub Flavored Markdown](https://guides.github.com/features/mastering-markdown/#GitHub-flavored-markdown).

# Licence

MIT