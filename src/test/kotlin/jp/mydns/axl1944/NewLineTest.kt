package jp.mydns.axl1944

import org.junit.Assert
import org.junit.Test

class NewLineTest {
    @Test
    fun standardNewLine() {
        val split = "Line1  \nLine2".split("\n")
        Assert.assertEquals("Line1<br>\nLine2", NewLine().parseNewLine(split).joinToString("\n"))
    }

    @Test
    fun manySpaces() {
        val split = "Line1                                                                \nLine2".split("\n")
        Assert.assertEquals("Line1<br>\nLine2", NewLine().parseNewLine(split).joinToString("\n"))
    }

    @Test
    fun oneSpace() {
        val split = "Line1 \nLine2".split("\n")
        Assert.assertEquals("Line1 \nLine2", NewLine().parseNewLine(split).joinToString("\n"))
    }

    @Test
    fun noNewLine() {
        val split = "Line1  Line1\nLine2".split("\n")
        Assert.assertEquals("Line1  Line1\nLine2", NewLine().parseNewLine(split).joinToString("\n"))
    }

    @Test
    fun twoSpacesNewLine() {
        val split = "Line1  Line1  \nLine2".split("\n")
        Assert.assertEquals("Line1  Line1<br>\nLine2", NewLine().parseNewLine(split).joinToString("\n"))
    }
}
