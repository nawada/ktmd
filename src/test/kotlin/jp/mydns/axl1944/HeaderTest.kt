package jp.mydns.axl1944

import org.junit.Test
import org.junit.Assert

class HeaderTest {
    @Test
    fun h1SetexShortestTest() {
        val split = "Header\n==".split("\n")
        Assert.assertEquals("<h1>Header</h1>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h1SetexLongTest() {
        val split = "Header\n================================================================================================================================".split("\n")
        Assert.assertEquals("<h1>Header</h1>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h1SetexNGTest() {
        val split = "Header\n=".split("\n")
        Assert.assertEquals("Header\n=", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h2SetexShortestTest() {
        val split = "Header\n--".split("\n")
        Assert.assertEquals("<h2>Header</h2>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h2SetexLongTest() {
        val split = "Header\n--------------------------------------------------------------------------------------------------------------------------------".split("\n")
        Assert.assertEquals("<h2>Header</h2>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h2SetexNGTest() {
        val split = "Header\n-".split("\n")
        Assert.assertEquals("Header\n-", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h1H2SetexCompositeTest1() {
        val split = "Header\n-=".split("\n")
        Assert.assertEquals("<h1>Header</h1>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h1H2SetexCompositeTest2() {
        val split = "Header\n=-".split("\n")
        Assert.assertEquals("<h2>Header</h2>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h1AtxTest() {
        val split = "# Header".split("\n")
        Assert.assertEquals("<h1>Header</h1>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h2AtxTest() {
        val split = "## Header".split("\n")
        Assert.assertEquals("<h2>Header</h2>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h3AtxTest() {
        val split = "### Header".split("\n")
        Assert.assertEquals("<h3>Header</h3>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h4AtxTest() {
        val split = "#### Header".split("\n")
        Assert.assertEquals("<h4>Header</h4>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h5AtxTest() {
        val split = "##### Header".split("\n")
        Assert.assertEquals("<h5>Header</h5>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun h6AtxTest() {
        val split = "###### Header".split("\n")
        Assert.assertEquals("<h6>Header</h6>", Header().parseHeader(split).joinToString("\n"))
    }

    @Test
    fun invalidAtxTest() {
        val split = "######## Header".split("\n")
        Assert.assertEquals("######## Header", Header().parseHeader(split).joinToString("\n"))
    }
}