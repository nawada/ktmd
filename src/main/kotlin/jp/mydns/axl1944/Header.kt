package jp.mydns.axl1944

import java.util.*

class Header {
    private var beforeLine: String = ""
    private val SETEX_EQUALS_REGEX = Regex("^[-=]+=$")
    private val SETEX_HYPHEN_REGEX = Regex("^[-=]+-$")
    private val ATX_REGEX = Regex("^(#{1,6}) (.+)$")

    /**
     * Parse headers
     * @param lines a markdown string
     * @return Parsed html
     */
    fun parseHeader(lines: List<String>): List<String> {
        val list = ArrayList<String>()
        for (line in lines) {
            val setexResult = parseSetex(line)
            if (setexResult.first) {
                list.removeAt(list.size - 1)
                list.add(setexResult.second)
            } else {
                list.add(parseAtx(line))
            }
            beforeLine = line
        }
        return list
    }

    /**
     * Setex use equal symbols and hyphen symbols for headers
     *
     * @param line
     * @return Pair<Boolean, ParsedString>
     */
    private fun parseSetex(line: String): Pair<Boolean, String> {
        if (SETEX_EQUALS_REGEX.matches(line)) {
            return Pair(true, "<h1>$beforeLine</h1>")
        } else if (SETEX_HYPHEN_REGEX.matches(line)) {
            return Pair(true, "<h2>$beforeLine</h2>")
        } else {
            return Pair(false, line)
        }
    }

    /**
     * Atx is use sharp symbol(s) for headers
     */
    private fun parseAtx(line: String): String {
        if (!ATX_REGEX.matches(line)) return line

        try {
            val founds = ATX_REGEX.findAll(line).map { it.groupValues }.toList()[0]
            val headerLevel = founds[1].length
            return "<h$headerLevel>${founds[2]}</h$headerLevel>"
        } catch (ex: NullPointerException) {
            System.err.println("Invalid line: $line")
            System.err.println("${ex.message}")
            return line
        }
    }
}