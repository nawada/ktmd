package jp.mydns.axl1944

import java.util.*

class NewLine {
    private val BR_REGEX = Regex("[\\W\\w]+ {2,}$")

    fun parseNewLine(lines: List<String>): List<String> {
        val list = ArrayList<String>()
        for(line in lines) {
            list.add(parse(line))
        }
        return list
    }

    private fun parse(line: String): String {
        if(BR_REGEX.matches(line)) {
            return line.replace(Regex(" {2,}$"), "<br>")
        }
        return line
    }
}
