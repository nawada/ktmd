package jp.mydns.axl1944

import java.io.File

fun main(args: Array<String>) {
    println(Ktmd.parse("Header\n--------------------"))
}

/**
 * This is GitHub Flavored Markdown Parser.
 * @author N.Wada
 */
class Ktmd {
    companion object {
        /**
         * Start parse markdown
         * @param item
         * @return Parsed html string
         */
        fun parse(item: Any): String {
            val self = Ktmd()
            val ret = when (item) {
                is String -> self.parse(item)
                is File -> self.parse(item)
                else -> ""
            }

            return ret
        }
    }

    private fun parse(str: String): String {
        val hasCr = str.indexOf('\r') >= 0
        val split = str.replace("\r", "").split("\n")
        var result = Header().parseHeader(split)
        result = NewLine().parseNewLine(result)
        return result.joinToString("\n")
    }

    private fun parse(file: File): String {
        return ""
    }
}
